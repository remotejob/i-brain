# mazurov


## WORKFLOW

## Create Namespace on Cluster

kubectl delete namespace i-brain-prod  
kubectl config set-context --current --namespace i-brain-prod

## Connect git repository to Kubernetes cluster

```
helm upgrade --install kubeagent gitlab/gitlab-agent \
    --namespace i-brain-prod \
    --create-namespace \
    --set image.tag=v15.6.0 \
    --set config.token=****************** \
    --set config.kasAddress=wss://kas.gitlab.com
```    
## Automatic SSL certificate
https://cert-manager.io/  
```
kubectl apply -f k8s/domain/cert-i-brain-ml.yml  
kubectl get certificate  
kubectl get event  
kubectl get challenges
```

## Deployment done by **GitOps** tecnology 
https://about.gitlab.com/topics/gitops/


## Persisten volume
manifest/prod/pvc.yaml  
(based on **longhorn deistributed** file system)  
https://longhorn.io/

## DEMOSITE(k8s service)
manifest/prod/i-brain-ml.yaml


## **Utils:** Copy static files to Persisten volume
```
kubectl apply -f utils/pod_for_migration.yaml   
kubectl exec --stdin --tty ubuntu -- /bin/bash  
kubectl cp  *******/dist ubuntu:longhorndisk -c ubuntu  
kubectl delete -f utils/pod_for_migration.yaml
```

